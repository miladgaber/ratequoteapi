﻿namespace Soap.API.Models.RQModels
{
    public class GetPalletTypeByPoints
    {
        public string MyRLCID { get; set; }
        public string originCity { get; set; }
        public string originZip { get; set; }
        public string destinationCity { get; set; }
        public string destinationZip { get; set; }

    }
}
