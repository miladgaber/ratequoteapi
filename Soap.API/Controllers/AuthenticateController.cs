﻿using Microsoft.AspNetCore.Mvc;
using Soap.API.Models;
using Soap.API.Services;
using System.Threading.Tasks;

namespace Soap.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticateController : ControllerBase
    {
        private readonly IUserService _userService;
        public AuthenticateController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _userService.RegisterUser(model);
                return result.IsSuccess ? Ok(result) : BadRequest(result);
            }
            return BadRequest("Some properties are not valid"); // Status code: 400
        }
        [HttpPost]
        [Route("register-admin")]
        public async Task<IActionResult> RegisterAdmin([FromBody] RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _userService.RegisterAdmin(model);
                return result.IsSuccess ? Ok(result) : BadRequest(result);
            }
            return BadRequest("Some properties are not valid"); // Status code: 400
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _userService.LoginUser(model);
                if (result.IsSuccess) return Ok(result);
                return BadRequest(result);
            }
            return BadRequest("Some properties are not valid");
        }
        [HttpPost]
        [Route("forget-password")]
        public async Task<IActionResult> ForgetPassword(string email)
        {
            if (string.IsNullOrEmpty(email)) return NotFound();
            var result = await _userService.ForgetPassword(email);
            if (result.IsSuccess) return Ok(result); return BadRequest(result); // 400
        }

        [HttpPost]
        [Route("reset-password")]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _userService.ResetPassword(model);
                if (result.IsSuccess) return Ok(result);
                return BadRequest(result);
            }
            return BadRequest("Some properties are not valid");
        }

    }
}
