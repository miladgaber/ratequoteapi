﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RateQuoteService;
using Soap.API.Models.RQModels;
using System.Threading.Tasks;

namespace Soap.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class RateQuoteController : ControllerBase
    {
        private readonly IConfiguration _configration;
        RateQuoteServiceSoapClient client;

        public RateQuoteController(IConfiguration configration)
        {
            _configration = configration;
            client = new RateQuoteServiceSoapClient(RateQuoteServiceSoapClient.EndpointConfiguration.RateQuoteServiceSoap12);

        }

        [HttpPost("Rate")]
        public async Task<IActionResult> GetRate(string APIKey, [FromBody] RateQuoteRequest obj)
        {
            if (ModelState.IsValid)
            {
                if (APIKey == null) APIKey = _configration["RateQuoteApiKey"];
                var result = await client.GetRateQuoteAsync(APIKey, obj);
                return Ok(result);
            }
            return BadRequest("Some properties are not valid");

        }

        [HttpGet("PalletTypes")]
        public async Task<IActionResult> GetPalletTypes(string APIKey)
        {
            if (ModelState.IsValid)
            {
                if (APIKey == null) APIKey = _configration["RateQuoteApiKey"];
                var result = await client.GetPalletTypesAsync(APIKey);
                return Ok(result);
            }
            return BadRequest();

        }
        [HttpGet("PalletTypeByPoints")]
        public async Task<IActionResult> GetPalletTypeByPoints(string APIKey, string MyRLCID, string originCity, string originZip, string destinationCity, string destinationZip)
        {
            if (ModelState.IsValid)
            {
                if (APIKey == null) APIKey = _configration["RateQuoteApiKey"];
                var result = await client.GetPalletTypeByPointsAsync(APIKey, MyRLCID, originCity, originZip, destinationCity, destinationZip);
                return Ok(result);
            }
            return BadRequest("Some properties are not valid");
        }

    }
}
