﻿using Soap.API.Models;
using System.Threading.Tasks;

namespace Soap.API.Services
{
    public interface IUserService
    {
        Task<Response> RegisterUser(RegisterModel model);
        Task<Response> RegisterAdmin(RegisterModel model);

        Task<Response> LoginUser(LoginModel model);

        Task<Response> ForgetPassword(string email);

        Task<Response> ResetPassword(ResetPasswordModel model);
    }
}
