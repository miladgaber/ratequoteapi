﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Soap.API.Models;
using Soap.Infrastructure.Identity;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Soap.API.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IEmailSender _emailSender;
        private readonly IConfiguration _configuration;

        public UserService(
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager,
            IEmailSender emailSender,
            IConfiguration configuration)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _emailSender = emailSender;
            _configuration = configuration;
        }

        public async Task<Response> RegisterUser(RegisterModel model)
        {
            if (model == null)
                throw new NullReferenceException("Reigster Model is null");

            var userExists = await _userManager.FindByEmailAsync(model.Email);
            if (userExists != null)
                return new Response
                {
                    IsSuccess = false,
                    Message = "User Registration Error!",
                    Errors = new List<string>() { " User already exists! " }
                };

            ApplicationUser user = new ApplicationUser()
            {
                Email = model.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = model.Username
            };
            var result = await _userManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
                return new Response { IsSuccess = false, Message = "User creation failed! Please check user details and try again." };

            return new Response { IsSuccess = true, Message = "User created successfully!" };
        }
        public async Task<Response> RegisterAdmin(RegisterModel model)
        {
            var emailExists = await _userManager.FindByEmailAsync(model.Email);

            var usernameExists = await _userManager.FindByNameAsync(model.Username);
            if (emailExists != null || usernameExists != null)
                return new Response
                {
                    IsSuccess = false,
                    Message = "Admin Registration Error!",
                    Errors = new List<string>() { " Already exists! " }
                };

            ApplicationUser user = new ApplicationUser()
            {
                Email = model.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = model.Username
            };

            var result = await _userManager.CreateAsync(user, model.Password);
            if (!result.Succeeded) new Response { IsSuccess = false, Message = "User creation failed! Please check user details and try again." };

            if (!await _roleManager.RoleExistsAsync(UserRoles.Admin))
                await _roleManager.CreateAsync(new IdentityRole(UserRoles.Admin));
            if (!await _roleManager.RoleExistsAsync(UserRoles.User))
                await _roleManager.CreateAsync(new IdentityRole(UserRoles.User));

            if (await _roleManager.RoleExistsAsync(UserRoles.Admin))
            {
                await _userManager.AddToRoleAsync(user, UserRoles.Admin);
            }

            return new Response { IsSuccess = true, Message = "User created successfully!" };
        }
        public async Task<Response> LoginUser(LoginModel model)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
            {
                var userRoles = await _userManager.GetRolesAsync(user);

                var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };

                foreach (var userRole in userRoles)
                {
                    authClaims.Add(new Claim(ClaimTypes.Role, userRole));
                }

                var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

                var token = new JwtSecurityToken(
                    issuer: _configuration["JWT:ValidIssuer"],
                    audience: _configuration["JWT:ValidAudience"],
                    expires: DateTime.Now.AddHours(3),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                    );


                string tokenAsString = new JwtSecurityTokenHandler().WriteToken(token);

                return new Response
                {
                    IsSuccess = true,
                    Message = tokenAsString,
                    ExpireDate = token.ValidTo
                };
            }
            return new Response { IsSuccess = false, Message = "Un Authrized! " };

        }

        public async Task<Response> ForgetPassword(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            if (user != null)
            {
                var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                var encodedToken = Encoding.UTF8.GetBytes(token);
                var validToken = WebEncoders.Base64UrlEncode(encodedToken);

                string url = $"{ _configuration["AppUrl"]}/ResetPassword?email={email}&token={validToken}";

                await _emailSender.SendEmailAsync(
                    "milad@milad-gaber.me",
                    user.Email,
                    "Mc Task - Reset Password",
                    $" Follow Link To Reset Your Password: {url} ");
                return new Response
                {
                    IsSuccess = true,
                    Message = " Reset password URL has been sent to email successfully!" +
                    " Url contain Email and Token :  " + url
                };
            }

            return new Response { IsSuccess = false, Message = "This Email Not Founded!" };
        }

        public async Task<Response> ResetPassword(ResetPasswordModel model)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (model.NewPassword != model.ConfirmPassword) return new Response { IsSuccess = false, Message = "Password Dosn't Match!" };


            var decodedToken = WebEncoders.Base64UrlDecode(model.Token);
            var normalToken = Encoding.UTF8.GetString(decodedToken);
            if (user != null)
            {
                var result = await _userManager.ResetPasswordAsync(user, normalToken, model.NewPassword);
                if (result.Succeeded) return new Response { IsSuccess = true, Message = "Password Has been reset Succesfully !" };
                return new Response { IsSuccess = false, Message = "Somesing Wrong !", Errors = result.Errors.Select(e => e.Description) };
            }
            return new Response { IsSuccess = false, Message = "This Email Not Founded!" };
        }
    }
}
