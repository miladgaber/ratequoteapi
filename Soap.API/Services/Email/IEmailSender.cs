﻿using System.Threading.Tasks;

namespace Soap.API.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string from, string to, string subject, string message);
    }
}
