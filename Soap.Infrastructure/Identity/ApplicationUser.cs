﻿using Microsoft.AspNetCore.Identity;

namespace Soap.Infrastructure.Identity
{
    public class ApplicationUser : IdentityUser
    {
    }
}
