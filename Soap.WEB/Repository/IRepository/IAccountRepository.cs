﻿using Soap.Infrastructure.Identity;
using System.Threading.Tasks;

namespace Soap.WEB.Repository.IRepository
{
    public interface IAccountRepository : IBaseRepository<ApplicationUser>
    {

        Task<ApplicationUser> LoginAsync(string url, ApplicationUser obj);
        Task<ApplicationUser> RegisterAsync(string url, ApplicationUser obj);
        Task<ApplicationUser> ForgetPasswordAsync(string url, ApplicationUser obj);
        Task<ApplicationUser> ResetPasswordAsync(string url, ApplicationUser obj);

    }
}
