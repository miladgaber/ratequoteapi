﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Soap.WEB.Repository.IRepository
{
    public interface IBaseRepository<T> where T : class
    {
        Task<T> GetAsync(string url, int Id, string token);
        Task<IEnumerable<T>> GetAllAsync(string url, string token);
        Task<bool> CreateAsync(string url, T objToCreate, string token);

    }
}
