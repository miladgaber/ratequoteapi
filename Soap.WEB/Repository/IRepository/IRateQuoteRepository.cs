﻿using Soap.API.Models.RQModels;
using System.Threading.Tasks;

namespace Soap.WEB.Repository.IRepository
{
    public interface IRateQuoteRepository
    {
        Task<bool> GetRateAsync(string ApiKey, GetRateQuote obj);

        Task<bool> GetPalletTypesAsync(string ApiKey);

        Task<bool> GetPalletTypeByPoints(string ApiKey, string MyRLCID, string originZip, string destinationCity, string destinationZip);
    }
}