﻿using Soap.Infrastructure.Identity;
using Soap.WEB.Repository.IRepository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Soap.WEB.Repository
{
    public class AccountRepository : IAccountRepository
    {
        public Task<bool> CreateAsync(string url, ApplicationUser objToCreate, string token)
        {
            throw new System.NotImplementedException();
        }

        public Task<ApplicationUser> ForgetPasswordAsync(string url, ApplicationUser obj)
        {
            throw new System.NotImplementedException();
        }

        public Task<IEnumerable<ApplicationUser>> GetAllAsync(string url, string token)
        {
            throw new System.NotImplementedException();
        }

        public Task<ApplicationUser> GetAsync(string url, int Id, string token)
        {
            throw new System.NotImplementedException();
        }

        public Task<ApplicationUser> LoginAsync(string url, ApplicationUser obj)
        {
            throw new System.NotImplementedException();
        }

        public Task<ApplicationUser> RegisterAsync(string url, ApplicationUser obj)
        {
            throw new System.NotImplementedException();
        }

        public Task<ApplicationUser> ResetPasswordAsync(string url, ApplicationUser obj)
        {
            throw new System.NotImplementedException();
        }
    }
}
