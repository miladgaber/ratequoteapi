﻿namespace Soap.WEB
{
    public static class SD
    {
        public const string APIBaseUrl = "https://localhost:44386/";
        public const string AuthPath = APIBaseUrl + "api/Authenticate";
        public const string RateQuotePath = APIBaseUrl + "api/RateQuote";
    }
}
